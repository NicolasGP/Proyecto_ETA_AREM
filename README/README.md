﻿# Escuela Colombiana de Ingeniería
## PROYECTO EMPRESARIAL FINAL
#### Arquitectura Empresarial - AREM

## Integrantes
```sh
Nicolás Guzmán Prieto
Santiago Chisco 
Andres Vega Torres
```

## Descripción
Este repositorio contiene los diferentes documentos del desarrollo practico de los contenidos estudiados en la clase de AREM aplicados al caso empresarial de Electronica Tabio (ETA). Una compañía dedicada a la  fabricación, mantenimiento y venta de controles para cercas eléctricas.  
El proyecto se ha desarrollado mediante diferentes entregas de acuerdo a los temas que vistos hasta ese momento, en este README es posible encontrar una pequeña descripción del contenido de cada entrega para mayor comprension del repositorio.

## Primera Entrega
En esta entrega se hizo el primer desarrollo del documento de arquitectura empresarial de Electronica Tabio (ETA) donde además se definió el alcance, StakeHolders y visión de la arquitectura. En este documento es posible encontrar:

- Contexto general del trabajo
- Modelos de referencia, vistas, puntos de vista y herramientas 
- Descripción de arquitectura base
- Descripción de arquitectura objetivo
- Análisis de brechas
- Portafolio de proyectos para llegar a la arq. objetivo

## Segunda Entrega 
En esta entrega se continuo con el desarrollo de la primera entrega, haciendo las correcciones pertinentes y complementando el documento de arquitectura empresarial con:
- La arquitectura de sistemas de información y datos
- La arquitectura de tecnologias
De igual forma, se elabora una nueva presentación enfocada en la explicación de las principales arquitecturas de esta entrega, sin embargo se retoman elementos previos con el fin de contextualizar los conceptos y facilitar el entendimiento de las propuestas.

## Entrega Final
Esta entrega es un compilado del trabajo desarrollado en las entregas anteriores, donde ya se han hecho las correcciones pertinentes de tal forma que se puedan encontrar tanto el informe ejecutivo como la presentacion en su mejor version.
Adicional al contenido mencionado anteriormente, se encuentra el modelo BPMN de un prototipo de tecnificacion del proceso de un nuevo diseño para control de cercas. Dicho prototipo se soporta con una aplicacion desplegada en heroku cuyo link tambien se puede econtrar siguiendo la ruta mencionada acontinuacion dentro del repositorio.

```sh
           
 	Prototipo --> Link App Heroku.txt

```